# Wemos Micropython Supersensor #

The Wemos Micropython Supersensor comprises several sensors which measures:

a) Temperature & Humidity - SHT30
b) Motion Detection - PIR
c) Light Intensity - BH1750
d) Air Quality - CSS811
e) Sound Detection - SparkFun


### Repository ###

This repository consists of the two different programming paradigms for the Supersensors. One is where the supersensors are programmed using an imperative style while the other uses an objected oriented style.

### Installation ###

These files should be installed ideally on an ESP8266 D1 Mini but should work with other ESP8266 Boards (not tested).

Easy installation can be accomplished using uPyCraft IDE. Also, it is mandatory to have an MQTT Broker setup and running in order for the code to run smoothly.

*** For best installation in using the object oriented style - upload the files in the folowing order:

sht30.py -> BH1750.py -> css.py -> umqttsimple.py -> mqtt.py -> heartbeat.py -> collector.py -> upload.py -> boot.py -> main.py




