def connect_mqtt():
  global client_id, mqtt_server
  client = MQTTClient(client_id, mqtt_server)
  #client = MQTTClient(client_id, mqtt_server, user=your_username, password=your_password)
  client.connect()
  print('Connected to %s MQTT broker' % (mqtt_server))
  return client
  
last_message = 0
message_interval = 10

air = carbon

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()  

def collect_sensor():
  global temperature, humidity, detection, lumination, audio
  
  temperature, humidity = temp.measure()
  detection = motion.value()
  lumination = light.luminance(BH1750.ONCE_HIRES_1)
  audio = sound.read()
  
  if (isinstance(temperature, float) and isinstance(humidity, float) or isinstance(detection, int) or isinstance(lumination, float) or isinstance(audio, int)): 
    temperature = ('{0:3.3f}'.format(temperature))
    humidity =  ('{0:3.3f}'.format(humidity))
    detection = ('{0:1.3f}'.format(detection))
    lumination = ('{0:3.3f}'.format(lumination))
    audio = ('{0:4.0f}'.format(audio))
    return temperature, humidity, detection, lumination, audio
     
try:
  client = connect_mqtt()
except OSError as e:
  restart_and_reconnect()
  
def collect_heartbeat():
  global temp_sensor, motion_sensor, light_sensor, air_sensor, sound_sensor
  
  temp_sensor = temp_status.is_present()
  motion_sensor = motion_status.value()
  light_sensor = light_status
  air_sensor = carbon
  sound_sensor = sound
  
  
  print('')
  print('Sensor Status')
  print('')
  
  if temp_sensor==True:
     print('SHT30 Sensor Status:', temp_status.is_present())
  else:
     print('SHT30 Sensor Status: Not Connected')
    
  if motion_sensor==1:
      print('PIR Sensor Status:', motion_status.value())
  else:
      print ('PIR Sensor Status: Not Connected')
      
  if light_sensor==i2c:
     print('Light Sensor Status:', light_status)
  else:
      print('Light Sensor Status: Not Connected')
      
  if air_sensor==carbon:
     print('Air Sensor Status:', carbon)
  else:
      print('Air Sensor Status: Not Connected')
      
  if sound_sensor==sound:
     print('Sound Sensor Status:', sound)
  else:
      print('Sound Sensor Status: Not Connected')
      
  return temp_sensor, motion_sensor, light_sensor, air_sensor, sound_sensor
  
  on(self)

while True:
  
  if (time.time() - last_message) > message_interval:
    
    temp_sensor, motion_sensor, light_sensor, air_sensor, sound_sensor = collect_heartbeat()
    temperature, humidity, detection, lumination, audio = collect_sensor()
   
    print('')
    print('Heartbeat Collection In Progress')
    print('')
    
    print(str(temp_sensor))
    print(str(motion_sensor))
    print(str(light_sensor))
    print(str(air_sensor))
    print(str(sound_sensor))
    
    client.publish(publish_sht30, str(temp_sensor))
    client.publish(publish_pir, str(motion_sensor))
    client.publish(publish_light, str(light_sensor))
    client.publish(publish_css, str(air_sensor))
    client.publish(publish_spark, str(sound_sensor))
    
    print('')
    print('Measurements Collection In Progress')
    print('')
    
    print(('Temperature:'), str(temperature))
    print(('Humidity:'), str(humidity))
    print(('Motion:'), str(detection))
    print(('Light:'), str(lumination))
    str(carbon.run())
    print(('Sound:'), str(audio))
    
    client.publish(publish_temperature, temperature)
    client.publish(publish_humidity, humidity)
    client.publish(publish_motion, detection)
    client.publish(publish_light, lumination)
    #client.publish(publish_air, carbon.run())
    client.publish(publish_sound, audio)
    
    last_message = time.time()














