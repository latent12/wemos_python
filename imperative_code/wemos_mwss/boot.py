# This file is executed on every boot (including wake-boot from deepsleep)

#import esp

#esp.osdebug(None)

#import webrepl

#webrepl.start()

try:
  import usocket as socket
except:
  import socket

import time
from umqttsimple import MQTTClient
import ubinascii
import machine
import micropython
import network
import esp
from machine import Pin
from machine import Pin, ADC
from sht30 import SHT30
from bh1750 import BH1750
import css
from machine import PWM, Pin, I2C

esp.osdebug(None)

i2c = machine.I2C(scl=machine.Pin(5), sda=machine.Pin(4))
light_status = i2c

gas_sensor_id = 'carbon'
carbon = GasSensor(id=gas_sensor_id, address=0x5A, scl_pin=2, sda_pin=0)

import gc
gc.collect()

ssid = ''
password = ''
mqtt_server = ''

client_id = ubinascii.hexlify(machine.unique_id())

publish_temperature = 'temperature' #temperature_topic
publish_humidity = 'humidity' #humidity_topic
publish_motion = 'motion' #motion_topic
publish_light = 'light' #light_topic
publish_air = carbon.run() #air_topic
publish_sound = 'sound' #sound_topic
publish_sht30 = 'sht30' #sht30_topic
publish_pir = 'pir' #pir_topic
publish_css = 'css' #css_topic
publish_spark = 'spark' #spark_topic

last_message = 0
message_interval = 10

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
  pass

print('Connection successful')
print(station.ifconfig())

temp = SHT30()
temp_status = SHT30()

motion = Pin(0, Pin.IN)
motion_status = Pin(0, Pin.IN)

light = BH1750(i2c, 0x23)

sound = ADC(0)












