class GasSensor:
    READ_DELAY = 10000
    ALG_RESULT_DATA_REGISTER = const(0X02)

    def __init__(self, id, address, scl_pin, sda_pin):
        self.id = id
        self.i2c = I2C(scl=Pin(scl_pin), sda=Pin(sda_pin))
        self.address = 0x5A

    def read(self):
        buf = bytearray(9)
        buf[0] = self.ALG_RESULT_DATA_REGISTER

        self.i2c.write(buf)
        self.i2c.readinto(buf)

        self.eCO2 = (buf[1] << 8) | (buf[2])
        self.TVOC = (buf[3] << 8) | (buf[4])
        
    def run(self):
        self.read()
        print('{} : CO2: {}'.format(self.id, self.eCO2, self.TVOC))



