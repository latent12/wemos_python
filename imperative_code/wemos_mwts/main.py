def connect_mqtt():
  global client_id, mqtt_server
  client = MQTTClient(client_id, mqtt_server)
  #client = MQTTClient(client_id, mqtt_server, user=your_username, password=your_password)
  client.connect()
  print('Connected to %s MQTT broker' % (mqtt_server))
  return client
  
last_message = 0
message_interval = 10

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()  

def collect_sensor():
  global temperature, humidity
  
  temperature, humidity = temp.measure()
  
  if (isinstance(temperature, float) and isinstance(humidity, float)): 
    temperature = ('{0:3.3f}'.format(temperature))
    humidity =  ('{0:3.3f}'.format(humidity))
    return temperature, humidity
     
try:
  client = connect_mqtt()
except OSError as e:
  restart_and_reconnect()
  
def collect_heartbeat():
  global temp_sensor
  
  temp_sensor = temp_status.is_present()
  
  print('')
  print('Sensor Status')
  print('')
  
  if temp_sensor==True:
     print('SHT30 Sensor Status:', temp_status.is_present())
  else:
     print('SHT30 Sensor Status: Not Connected')
      
  return temp_sensor

while True:
  
  if (time.time() - last_message) > message_interval:
    
    temp_sensor = collect_heartbeat()
    temperature, humidity = collect_sensor()
    
    
    print('')
    print('Heartbeat Collection In Progress')
    print('')
    
    print(str(temp_sensor))
    
    client.publish(publish_sht30, str(temp_sensor))
    
    print('')
    print('Measurements Collection In Progress')
    print('')
    
    print(('Temperature:'), str(temperature))
    print(('Humidity:'), str(humidity))
    
    client.publish(publish_temperature, temperature)
    client.publish(publish_humidity, humidity)
    
    last_message = time.time()
