class Collect_Data:

    @staticmethod
    def collect_sensor():
        global temperature, humidity, detection, lumination, audio
  
        temperature, humidity = temp.measure()
        detection = motion.value()
        lumination = light.luminance(BH1750.ONCE_HIRES_1)
        audio = sound.read()
  
        if (isinstance(temperature, float) and isinstance(humidity, float) or isinstance(detection, int) or isinstance(lumination, float) or isinstance(audio, int)): 
            temperature = ('{0:3.3f}'.format(temperature))
            humidity =  ('{0:3.3f}'.format(humidity))
            detection = ('{0:1.3f}'.format(detection))
            lumination = ('{0:3.3f}'.format(lumination))
            audio = ('{0:4.0f}'.format(audio))
            return temperature, humidity, detection, lumination, audio

